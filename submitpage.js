var codeFile = null;
function upFile(name) {
    $(name).click();
}
const onFileChange = e => {
    console.log(e);
    console.log(e.files[0]);
    const file = e.files[0];
    if (!file) {
        return alert(1);
    }
    codeFile = e.files[0];
    let fileName = file.name;
    console.log(fileName);
    changeFileTail(fileName);
    const reader = new FileReader();
    reader.onload = async (e) => {
        const code = (e.target.result);
        $('#editor').html(code);
        Prism.highlightAll();
    };
    reader.readAsText(file);
}
function changeFileTail(fileName) {
    let a = fileName.split('.');
    console.log(a);
    let s = "language-" + a[1];
    $('#editor').attr("class", s);
}
function getData() {
    let s = "";
    s += $("#contestant-id").val();
    alert(s);
}
function getRunSampleLink() {
    let contestantId = $("#contestant-id").val();
    let password = $("#password").val();
    let problemName = $("#problem-name").val();
    let res = "http://192.168.10.87:6905/api/v2/run_samples?";
    res += "contestant_id=" + contestantId + "&";
    res += "password=" + password + "&";
    res += "problem_name=" + problemName;
    return res; 
}
function getSummitLink() {
    let contestantId = $("#contestant-id").val();
    let password = $("#password").val();
    let problemName = $("#problem-name").val();
    let res = "http://192.168.10.87:6905/api/v2/submit?";
    res += "contestant_id=" + contestantId + "&";
    res += "password=" + password + "&";
    res += "problem_name=" + problemName;
    return res; 
}
function runTestSample() {
    let contestantId = $("#contestant-id").val();
    let password = $("#password").val();
    let problemName = $("#problem-name").val();
    if (contestantId.length == 0 || password.length == 0 || problemName.length == 0) {
        alert("Hãy nhập đủ dữ liệu!");
        return;
    }
    let link = getRunSampleLink();
    sendData(link);
}

function sendData(link) {
    let fd = new FormData();    
    fd.append( 'file', codeFile);
    console.log("codeFile : ");
    console.log(codeFile);
    $.ajax({
    url: link,
    data: fd,
    processData: false,
    contentType: false,
    type: 'POST',
    success: function(data){
        alert(data.message);
        console.log(data);
    }
    });
}   
function switchPage() {
    location1 = "submitpage.html";
    location2 = "startpage.html";
    let curUrl = window.location.toString();
    // alert(curUrl);
    if (curUrl.includes(location2)){
        window.location.href=location1;
    } else if (curUrl.includes(location1)){
        window.location.href=location2;
    }  else {
        window.location.href=location2;
    }               
}