function initPoint(data) {
    // console.log(data);
    for (let i in data) {
        data[i].num_successful_hacks = 0;
        data[i].num_unsuccessful_hacks = 0;
        data[i].point = 0;
        data[i].A = {
            "point" : 0,
            "num_submissions": 0,
            "best_submission": 0,
            "submit_time": 0
        }
        data[i].B = {
            "point" : 0,
            "num_submissions": 0,
            "best_submission": 0,
            "submit_time": 0
        }
        data[i].C = {
            "point" : 0,
            "num_submissions": 0,
            "best_submission": 0,
            "submit_time": 0
        }
        // console.log(data[i]);
    }
    return data;
}
function createTablePart(name) {
    let tablePart = $("<div></div>",{
        "id" : "table-part-" + name,
        "class" : "table-part"
    });
    $("#container").append(tablePart);
}
function insertTextClassification() {
    $('.table-name.A').prepend("TABLE A");
    $('.table-name.B').prepend("TABLE B");
    $('.tag-order-number').prepend("#");
    $(".tag-id").prepend("ID");
    $(".tag-name").prepend("NAME");
    $(".tag-points").prepend("POINTS");
    $(".problems.A").prepend("A");
    $(".problems.B").prepend("B");
    $(".problems.C").prepend("C");
}
function insertTextCompetitive() {
    $('.tag-order-number').prepend("#");
    $(".tag-id").prepend("ID");
    $(".tag-name").prepend("NAME");
    $(".tag-points").prepend("POINTS");
    $(".tag-hacks").prepend("HACKS");
    $(".problems.A").prepend("A");
    $(".problems.B").prepend("B");
    $(".problems.C").prepend("C");
}
function createTable(tableName) {
    createTablePart(tableName);
    let divTableName = $("<div></div>",{
        "class" : "table-name " + tableName
    });
    let s = "#table-part-" + tableName;
    $(s).append(divTableName);
    
    let table = $("<table></table>",{
        "class" : "table " + tableName,
        "id" : "table" + tableName,
        "border" : "2"
    });
    s = ".table-name." + tableName;
    // console.log(s);
    $(s).append(table);
    let tr = $("<tr></tr>",{
        "class" : "header-row " + tableName
    });
    s = ".table." + tableName;
    $(s).append(tr);
    
    let thTag = $("<th></th>",{
        "class" : "info-col tag-order-number"
    });
    s = ".header-row." + tableName;
    $(s).append(thTag);

    let thId = $("<th></th>",{
        "class" : "info-col tag-id"
    });
    $(s).append(thId);

    let thName = $("<th></th>",{
        "class" : "info-col tag-name"
    });
    $(s).append(thName);
    
    let thPoints = $("<th></th>",{
        "class" : "info-col tag-points"
    });
    $(s).append(thPoints);

    let thProA = $("<th></th>",{
        "class" : "info-col problems A"
    });
    $(s).append(thProA);

    let thProB = $("<th></th>",{
        "class" : "info-col problems B"
    });
    $(s).append(thProB);

    let thProC = $("<th></th>",{
        "class" : "info-col problems C"
    });
    $(s).append(thProC);
}
function generateCompetitive() {
    let table = $("<table></table>",{
        "border" : "1",
        "id" : "C-table"
    });
    $("#C-container").append(table);

    let tr = $("<tr></tr>",{
        "class" : "C-table-head"
    });
    $("#C-table").append(tr);

    let thTag = $("<th></th>",{
        "class" : "C-info-col tag-order-number"
    });
    $(".C-table-head").append(thTag);

    let thId = $("<th></th>",{
        "class" : "C-info-col tag-id"
    });
    $(".C-table-head").append(thId);

    let thName = $("<th></th>",{
        "class" : "C-info-col tag-name"
    });
    $(".C-table-head").append(thName);
    
    let thPoints = $("<th></th>",{
        "class" : "C-info-col tag-points"
    });
    $(".C-table-head").append(thPoints);

    let thHacks = $("<th></th>",{
        "class" : "C-info-col tag-hacks"
    });
    $(".C-table-head").append(thHacks);

    let thProA = $("<th></th>",{
        "class" : "C-info-col problems A"
    });
    $(".C-table-head").append(thProA);

    let thProB = $("<th></th>",{
        "class" : "C-info-col problems B"
    });
    $(".C-table-head").append(thProB);

    let thProC = $("<th></th>",{
        "class" : "C-info-col problems C"
    });
    $(".C-table-head").append(thProC);
}
function fillBoardJquery(id, tableType, data) {
    let index = 1;
    // console.log(tableType);
    data = Object.values(data);
    // data = sortDataByRank(data);
    data = sortDataByPoint(data);
    console.log("data : ");
    console.log(data);
    for (let i = 0 ; i < data.length ; ++i) {
        let item = data[i], s = "";
        // console.log("item : ");
        // console.log(item);
        // if (tableType != "table") {
        //     if(tableType == "A" && item.group != "A") continue;
        //     if(tableType == "B" && item.group != "B") continue;
        // } 
        // console.log("START!");
        let trElement = $("<tr></tr>",{
            "id" : "row" + item.contestant_id
        });
        let orderNumber = $("<td></td>");
        if (tableType == "table") orderNumber.text(i + 1);
        else {
            orderNumber.text(index);
            index++;
        }
        let tdId = $("<td></td>",{
            "id" : "id" + item.contestant_id
        });
        let tdName = $("<td></td>",{
            "id" : "tdName" + item.contestant_id
        });
        let tdPoint = $("<td></td>",{
            "id" : "tdPoint" + item.contestant_id
        });
        // let tdHacks = $("<td></td>",{
        //     "id" : "tdHacks" + item.contestant_id
        // });
        let tdProA = $("<td></td>",{
            "id" : "ProA" + item.contestant_id,
            "class" : "tdProblem"
        });
        let tdProB = $("<td></td>",{
            "id" : "ProB" + item.contestant_id,
            "class" : "tdProblem"
        });
        let tdProC = $("<td></td>",{
            "id" : "ProC" + item.contestant_id,
            "class" : "tdProblem"
        });

        tdId.text(item.contestant_id);    
        tdName.text(item.name);
        tdPoint.text(item.point);
        let tdHacks = fillHackCol(item.num_successful_hacks, item.num_unsuccessful_hacks, item.contestant_id);
        if (item.A.num_submissions == 0) tdProA.text("");
        else {
            if (item.A.best_submission == -1) {
                tdProA.html(fillFailedSubmit(item.contestant_id, "A", item.num_submissions));
            } else {
                tdProA.html(fillAcceptedSubmit(item.contestant_id, "A", item.A.submit_time, item.num_submissions, item.A.point));
            }
        }
        if (item.B.num_submissions == 0) tdProB.text("");
        else {
            if (item.B.best_submission == -1) {
                tdProB.html(fillFailedSubmit(item.contestant_id, "B", item.num_submissions));
            } else {
                tdProB.html(fillAcceptedSubmit(item.contestant_id, "B", item.B.submit_time, item.num_submissions, item.B.point));
            }
        }
        if (item.C.num_submissions == 0) tdProC.text("");   
        else {
            if (item.C.best_submission == -1) {
                tdProC.html(fillFailedSubmit(item.contestant_id, "C", item.num_submissions));
            } else {
                tdProC.html(fillAcceptedSubmit(item.contestant_id, "C", item.C.submit_time, item.num_submissions, item.C.point));
            }
        }
        trElement.append(orderNumber);
        trElement.append(tdId);
        trElement.append(tdName);
        trElement.append(tdPoint);
        if (tableType == "table") trElement.append(tdHacks);
        trElement.append(tdProA);
        trElement.append(tdProB);
        trElement.append(tdProC);
        // console.log(tdProA);
        // console.log(tdProB);
        // console.log(tdProC);
        s = "#row" + item.contestant_id;
        $(s).append(tdId);
        s = "#" + id;
        $(s).append(trElement);
        // console.log("DONE!");
    }
}
function initDataPoint(problemName, contestant_id) {
    let tdPoint = $("<td></td>",{
        "id" : "DataProblem" + problemName + contestant_id
    }).text("");
    tdPoint.css("backgroundColor", "#b2bec3");
    return tdPoint;
}
function fillFailedSubmit(contestant_id, name, cnt) {
    let s = "#Pro" + name + contestant_id;

    let span = $("<span></span>").text("-" + cnt);
    $(s).css("background-color", "#f37474");
    return span;
}
function fillAcceptedSubmit(contestant_id, name, dataTime, cnt, point) {
    let s = "#Pro" + name + contestant_id;
    $(s).css("background-color", "#58b323");
    let divPoint = $("<div></div>");
    divPoint.text(point).css("color", "white");
    let divTime = $("<div></div>");
    let hour = Math.floor(dataTime / 60);
    let minute = dataTime % 60;
    if(minute < 10) minute = "0" + minute;
    let time = hour + ":" + minute;
    if(cnt > 1) time += "/" + cnt;
    divTime.text(time);
    divTime.css("font-size", "15px");
    let bigDiv = $("<div></div>");
    bigDiv.append(divPoint);
    bigDiv.append(divTime);
    bigDiv.css("width", "100%");
    bigDiv.css("height","100%");
    bigDiv.css("background-color", "#58b323");
    return bigDiv;
    // $(s).html("");
    // $(s).append(divPoint);
    // $(s).append(divTime);
    // $(s).css("background-color", "");
}
function changeDataProblem(data) {
    let contestant_id = data.contestant_id;
    let name = data.problem;
    let s = "#Pro" + name + contestant_id;
    let totalPoint = "#tdPoint" + contestant_id;
    let oldPoint = $(totalPoint).text();
    $(totalPoint).html(data.total_point);
    if (data.status == "0" || data.status == "-1") {
        if (data.total_point != oldPoint) {
            $(s).html(fillFailedSubmit(contestant_id, name, data.num_submissions));
        } else {
            $(s).html(fillAcceptedSubmit(contestant_id, name, data.time, data.num_submissions, data.point));
        }
    } else { //(data.status == "1")
        $(s).html(fillAcceptedSubmit(contestant_id, name, data.time, data.num_submissions, data.point));
    }
}
function fillHackCol(suc, unsuc, id) {
    let tdHacks = $("<td></td>",{
        "id" : "tdHacks" + id
    });
    let SucHacks = $("<div></div>");
    let UnsucHacks = $("<div></div>");
    if (suc > 0) {
        SucHacks.append(suc + "");
        SucHacks.css("color", "green");
        tdHacks.append(SucHacks);
    } 
    if(unsuc > 0) {   
        UnsucHacks.append("-" + unsuc);
        UnsucHacks.css("color", "red");
        tdHacks.append(UnsucHacks);
    }
    return tdHacks;
}
function changeData(oldData, newData) {
    for(let i = 0 ; i < oldData.length ; ++i) {
        if(oldData[i].contestant_id == newData.contestant_id) {
            console.log("NewData : ");
            console.log(newData);
            oldData[i].point = newData.total_point;
            oldData[i][newData.problem].point = newData.point;
            oldData[i][newData.problem].submit_time = newData.time;
            oldData[i][newData.problem].num_submissions = newData.num_submissions;
            //Chưa add data mớiiiiiiiiiiiiiiiiiiiii
         }
    }
    return sortDataByPoint(oldData);
}
function sortDataByPoint(data) {
    data.sort((a, b) => {
        return b.point - a.point;
    });
    return data;
}
function delHTML(type) {
    console.log("DEL");
    if (type == 1) {
        $("#C-container").children().remove();
    } else {
        $("#container").children().remove();
    }
}
function switchPage() {
    location1 = "submitpage.html";
    location2 = "startpage.html";
    let curUrl = window.location.toString();
    // alert(curUrl);
    if (curUrl.includes(location2)){
        window.location.href=location1;
    } else if (curUrl.includes(location1)){
        window.location.href=location2;
    }  else {
        window.location.href=location2;
    }               
}
function sortDataByRank(data) {
    data.sort((a, b) => {
        return b.rank - a.rank;
    });
    return data;
}
function requestData() {
    let link = "http://192.168.10.87:6905/api/public/sc_pusher";
    $.ajax({
        url: link,
        data: {},
        processData: false,
        contentType: false,
        type: 'GET',
        success: function(data){
            alert(data.status);
            console.log(data);
        }
    });
}
// 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 181 9 20
 
// 1 2 4 5 7 8 10 11 13 14 16 17

// function fillBoard(id, tableType, data){
//     // console.log("data : " + data);
//     let index = 1;
//     for (let i = 0; i < data.length; ++i) {
//         let item = data[i];
//         if (tableType != "table") {
//             if(tableType == "A" && item.group != "A") continue;
//             if(tableType == "B" && item.group != "B") continue;
//         } 
//         let trElement = document.createElement("tr");
//         let orderNumber = document.createElement("td");
//         if (tableType == "table") orderNumber.innerHTML = i;
//         else {
//             orderNumber.innerHTML = index;
//             index++;
//         }
//         let tdId = document.createElement("td");
//         tdId.innerHTML = item.contestant_id;
//         tdId.setAttribute("id", item.contestant_id);
//         let tdName = document.createElement("td");  
//         tdName.innerHTML =item.name;
//         tdName.setAttribute("id", "tdName" + item.contestant_id);
//         let tdPoints = document.createElement("td");
//         tdPoints.innerHTML = item.point;
//         tdPoints.setAttribute("id", "tdPoints" + item.contestant_id);
//         trElement.appendChild(orderNumber);
//         trElement.appendChild(tdId);
//         trElement.appendChild(tdName);
//         trElement.appendChild(tdPoints);
//         if (tableType == "table") {
//             let tdHacks = fillHackCol(item.num_successful_hacks, item.num_unsuccessful_hacks);
//             tdHacks.setAttribute("id", "tdHacks" + item.contestant_id);
//             trElement.appendChild(tdHacks);
//         }
//         trElement.appendChild(initDataPoint("A", item.contestant_id));
//         // trElement.appendChild(addDataProblem(item.A, "A",item.contestant_id));
//         trElement.appendChild(addDataProblem(item.B, "B",item.contestant_id));
//         trElement.appendChild(addDataProblem(item.C, "C",item.contestant_id));
//         if (tableType == "C-table") {
//             let table = document.getElementById(id);
//             table.appendChild(trElement);
//         } else {
//             let table = document.getElementById(id);
//             table.appendChild(trElement);
//         }
//     }
// }